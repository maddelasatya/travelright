<?php include 'staticheader.php';?>

<div id="content1">
<div id="content2">
<h1 style="font-size:20px;text-align:left;font-weight:900">Cancellation Policy: </h1>


<ol class="oltext" style="list-style-type:decimal;">
<li>
The cancellation terms are different for each bus operator and are set by the bus operator itself. These terms are shown while booking as well as on the ticket printout/Email confirmation.</li>

<li>The cut-off time for canceling a bus ticket depends on the bus starting point time and not on a particular boarding point time. [For example If the bus starting point time is 6pm and your boarding point time is 8pm, the cancellation cut-off times are applicable for 6pm and not 8pm]
</li>
<li>Tickets booked on TravelRight.in can be cancelled up to 2 hrs prior to the scheduled departure time To amend/cancel your ticket offline, call TravelRight Customer Care on                      and also please check with TravelRight�s Customer Help Desk for the exact cancellation rules on the ticket you have purchased. No cancellation is possible within two hours of departure. Some bus operator do not allow ticket to be cancelled much before two hours of departure. In that case cancellation shall not be possible on TravelRight.in within those hours of departure.
</li>

<li>The following is the cancellation fee: Between 24 hours to 3 days before journey the cancellation charge is 10%. Between 3 days to 1 week before journey the cancellation charge is 5%.
</li>
<li>Tickets booked online should be cancelled online only.
</li>
<li>Cancellation charges are applicable on original fare but not on the discounted fare. The cancellation charges are deducted from the collected/discounted fare and the balance is refunded. [For example: If the original ticket fare is Rs.1000 and your discounted fare is Rs.900, the cancellation charges are calculated on the original fare. i.e., on Rs.1000. So if the cancellation charges are 10%, your refund amount would be Rs.900-100 - 15 = Rs.785.]
</li>
<li>In case of ticket cancellation, Insurance amount is non-refundable.
</li>
<li>Preponement or Postponement cannot be done once ticket is confirmed.
</li>
<li>Service tax amount wont be refunded back to the customers.
<br/><br/>
If the customer cancels the bus booking, TravelRight will refund the purchase price after deducting the discount amount, and any other applicable bank and cancellation charges / penalty.
</li>
<li>Refunds processed for cancelled tickets will be transferred to the passenger's bank account, debit / credit card.
</li>
<li>Any refunds can happen within 3 to 7 working days.</li>
</ol>
<h1 style="font-size:20px;text-align:left;font-weight:900">Refunds</h1>
<p class="terms">
After booking of ticket, user should verify the details immediately. In case of any discrepancies, ticket should be cancelled within 10 minutes from the time of booking to avail full refund.
</p>

</div>

</div>
<?php include 'footer.php'; ?>