<?php include 'header.php';?>

<div class="wrap">
<h1 class="passenger">Passenger Details</h1>
<div class="passenger-form">
<form name="pass-details">
<div class="addform">

		<label for="username">Name <span class="color-mark">*</span> </label>
		<label for="usergen">Gender <span class="color-mark">*</span></label>
		<label for="userage">Age <span class="color-mark">*</span></label>
			<?php
			$seats=$_GET['seats'];
			$splitarray = explode(",",$seats);
			for ($i = 0; $i < count($splitarray); $i++) {	
			?>
			<div class="tabdet">
				<input type="hidden" id="userid" class="userid" name="userid" placeholder="Enter your Name" />
				<input type="text" id="username" class="username" name="username" placeholder="Enter your Name" />
				<span id="radiogen">
				<input type="radio" id="usergen" class="usergen" name="usergen_<?php echo $i; ?>" value="um" />&nbsp;Male&nbsp;&nbsp;
				<input type="radio" id="usergen" class="usergen" name="usergen_<?php echo $i; ?>" value="uf"  />&nbsp;Female
			    </span>
				<input type="text" id="userage" class="userage" name="userage" value="" placeholder="Age" />
			</div>
			<?php  } ?>
	
	</div>
<ul class="ulform">

<h1 class="passenger">Contact Details</h1>


	<li>
		<label for="useremail">Email <span class="color-mark">*</span>: </label>
			<div class="divformfield">
				<input type="text" id="useremail" class="useremail" name="useremail" placeholder="Enter your Email" />
			</div>
	</li>
	<li>
		<label for="usergen">Mobile <span class="color-mark">*</span>:</label>
			<div class="divformfield">
				<input type="text" id="usermob1" class="usermob1" name="usermob1" value="+91" disabled="disabled" />
				<input type="text" id="usermob" class="usermob" name="usermob" value="" placeholder="Enter Mobile Number" />
				
			</div>
	</li>
	<li>
		<label for="usernumer">Alt Number <span class="color-mark">*</span>: </label>
			<div class="divformfield">
				<input type="text" id="usernumer" class="usernumer" name="usernumer" value="" placeholder="Alternate Number" />
			</div>
	</li>
	<li>
		<label for="useraddress">Address <span class="color-mark">*</span>: </label>
			<div class="divformfield">
				<textarea id="useraddress" class="useraddress" name="useraddress" value="" ></textarea>
			</div>
	</li>
	<li>
		<label for="userpincode">Pin Code <span class="color-mark">*</span>: </label>
			<div class="divformfield">
				<input type="text" id="userpincode" class="userpincode" name="userpincode" value="" placeholder="Enter Pin Code" />
			</div>
	</li>
<h1 class="passenger">Verification Details</h1>
<li>
		<label for="useridtype">ID type <span class="color-mark">*</span>: </label>
			<div class="divformfield">
				<select id="useraddress" class="useraddress" name="useraddress" >
					<option value=""><--Select id proof--></option>
					<option value="DrLicense">Driving license</option>
					<option value="PanCard">PAN card</option>
					<option value="Passport">Passport</option>
					<option value="RationCard">Ration card</option>
					<option value="VoterIdCard">Voter ID card</option>
					<option value="AadhaarCard">Aadhaar card</option>
				</select>
			</div>
	</li>
	<li>
		<label for="useridnumer">ID Number <span class="color-mark">*</span>: </label>
			<div class="divformfield">
				<input type="text" id="useridnumer" class="useridnumer" name="useridnumer" value="" placeholder="ID Number" />
			</div>
	</li>
	<li>
		<label for="usernameid">Name on ID <span class="color-mark">*</span>: </label>
			<div class="divformfield">
				<input type="text" id="usernameid" class="usernameid" name="usernameid" value="" placeholder="Name on ID" />
			</div>
	</li>
	<li>
		<label for="userdoctor">I am a Doctor </label>
			<div class="divformfield tabfield">
				<input type="radio" name="userdoctor" id="userdoctor"  value="yes">&nbsp;Yes&nbsp;&nbsp;
				<input type="radio" name="userdoctor" id="userdoctor" value="no">&nbsp;No
		
		</div>
	</li>
	<li id="designation">
		<label for="userdesignation">Designation : </label>
			<div class="divformfield">
				<input type="text" name="userdesignation" id="userdesignation" value="" Placeholder="Enter Your Designation " />
				
		</div>
	</li>
	<li>
		<label for="usersubmit"></label>
			<div class="divformfield submit1">
				<input type="submit" id="usersubmit" class="usersubmit" name="usersubmit" value="PROCEED TO PAYMENT"  />
				<input type="reset" id="userreset" class="usersubmit" name="userreset" value="CLEAR ALL"  />
			
			</div>
	</li>
</ul>

</form>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  
	       $("input[name=userdoctor]:radio").bind("change", function(event, ui) {
			if ($(this).val() == 'yes') {
				 $("#designation").show("slow");
        } else {
				 $("#designation").hide("slow");
        }

                                                            // Call function here
                                                        });
});
</script>
<?php include 'footer.php'; ?>