<?php include 'staticheader.php';?>

<div id="content1">
<div id="content2">
<h1 style="font-size:20px;text-align:left;font-weight:900">Who are we?</h1>

<p class="terms">
TravelRight.in is India's online travel company. Positioned as a brand that believes in " ", we 
provide information, pricing, availability, and booking facility for  bus  reservations. We offer a host
of travel services designed to make business and leisure travel easier.
<br/><br/>
Based in Bangalore, India, TravelRight is a one-stop-shop for all travel-related services.Founded in the
year 2015, TravelRight comprises of a passionate bunch of techies,  content curators, marketing folks 
and designers.
<br/><br/>
</p>
<h1 style="font-size:20px;text-align:left;font-weight:900">What we do</h1>
<p class="terms">
  TravelRight works towards giving travellers a one-stop-shop to get all their travel related answers. 
Right from choosing your travel destination to nitty-gritties like what to see, where to eat, where to 
stay, tips and trivia, you can plan all elements of your perfect trip here. We'll be your friend, 
philosopher and guide with reliable information for every trip you undertake, with online tools and 
mobile apps making your lives simple and stress-free.   
<br/><br/>
You can also follow TravelRight.in on:<br/><br/>
<a href="#" style="color:#000">http://www.facebook.com/TravelRight</a>
</p>
</div>
</div>
<?php include 'footer.php'; ?>