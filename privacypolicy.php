<?php include 'staticheader.php';?>

<div id="content1">
<div id="content2">
<h1 style="font-size:20px;text-align:left;font-weight:900">Privacy Policy:</h1>

<p class="terms">
We at TravelRight, value the relationship we have with our customers and web site visitors, and are committed to responsible information and handling practices. We take the privacy of our customers very seriously and want you to feel comfortable whenever you visit our web site.<br/><br/>
TravelRight respects your privacy and recognizes the need to protect the personally identifiable information (any information by which you can be identified, such as name, address, and telephone number) you share with us. In general, you can visit <b style="font-weight: 900;color:#41BC9B">TravelRight.in</b> website without telling us who you are or revealing any personal information about yourself.TravelRight.in will attempt to respond to all reasonable concerns or inquiries within five business days of receipt. 
<br/><br/>
Thank you for using TravelRight!
</p>
</div>
</div>
<?php include 'footer.php'; ?>