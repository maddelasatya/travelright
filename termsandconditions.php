<?php include 'staticheader.php';?>

<div id="content1">
<div id="content2">
<h1 style="font-size:20px;text-align:left;font-weight:900">Terms and conditions:</h1>

<p class="terms">
TravelRight is an online bus ticketing service. It does not operate its own bus services. TravelRight provides live bus chart status to the travelers and a choice of bus operators, departure times and prices. It has tied up with many bus operators to provide these services to customers.<br/><br/>
TravelRight's advice to customers is to choose the bus operators that they are aware of and whose services they are comfortable with. <br/><br/>

<ol class="oltext" style="list-style-type:decimal;">
<li>
The arrival and departure times mentioned on the ticket are only tentative timings . However the bus will not leave the source before the time that is mentioned on the ticket.
</li>
<li>Passengers are required to furnish the following at the time of boarding the bus:<br/>
<span>- </span>A copy of the ticket (A print out of the ticket or the print out of the ticket e-mail).<br/> 
<span>- </span>Identity proof (Driving license, Student ID card, Company ID card, Passport, PAN card or Voter ID card).
</li>
<li>Change of bus: In case the bus operator changes the type of bus due to some reason, TravelRight will refund the differential amount to the customer upon being intimated by the customers in 24 hours of the journey.</li>
<li>Passengers hereby consent to receive communications by SMS or calls from TravelRight with regard to services provided by them</li>

<li>Bus fares shown on the website are subject to change. Bus fares may also be subject to additional charges and fees including taxes.</li>
<li>Please note that the cancellation fee and cancellation period may differ from one bus operator to another. Please contact any of our executives for complete details or enter your ticket number on the print ticket tab to read the cancellation policy for your ticket.</li>
</ol>
</p>

<h1 style="font-size:20px;text-align:left;font-weight:900">TravelRight responsibilities include:</h1><br/>
<ul class="ultext">
<li>Issuing a valid ticket (a ticket that will be accepted by the bus operator) for its network of bus operators.</li>
<li>Providing refund and support in the event of cancellation.</li>
<li>Providing customer support and information in case of any delay(s) / inconvenience.</li>
<li>TravelRight is not responsible for any accidents or harm caused to the passenger and is hereby excused from any liability or expense arising from claims, losses, damages.</li>
<li>Maximum of Five (5) passengers can book seats in a single ticket.</li>

</ul>
</div>
</div>
<?php include 'footer.php'; ?>